mistral-dashboard (15.0.0-2) unstable; urgency=medium

  * Fix Python 3.11 compat (Closes: #1024958):
    - add py3.11-getargspec-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Dec 2022 10:22:47 +0100

mistral-dashboard (15.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 23:13:37 +0200

mistral-dashboard (15.0.0~rc1-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed patches applied upstream:
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Sep 2022 16:23:00 +0200

mistral-dashboard (14.0.0-2) unstable; urgency=medium

  * Added Django 4 compat (Closes: #1015137):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Jul 2022 14:43:39 +0200

mistral-dashboard (14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 21:20:35 +0200

mistral-dashboard (14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 13:58:37 +0200

mistral-dashboard (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 19:28:44 +0100

mistral-dashboard (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:35:17 +0200

mistral-dashboard (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:51:33 +0200

mistral-dashboard (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (Build-)depends on minimum horizon >= 20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 15:01:07 +0200

mistral-dashboard (12.0.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:21:46 +0200

mistral-dashboard (12.0.1-1) experimental; urgency=medium

  * New upstream version.

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 15 Jul 2021 13:00:21 +0200

mistral-dashboard (12.0.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 11:48:31 +0200

mistral-dashboard (12.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:31:45 +0200

mistral-dashboard (12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 13:14:46 +0200

mistral-dashboard (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Sun, 28 Mar 2021 13:00:09 +0200

mistral-dashboard (11.0.0-2) unstable; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Jul 2021 11:49:29 +0200

mistral-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 10:44:15 +0200

mistral-dashboard (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-mock from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 22:20:03 +0200

mistral-dashboard (10.0.0-1) unstable; urgency=medium

  * Fixed watch file URL.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 16:48:12 +0200

mistral-dashboard (10.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:19:35 +0200

mistral-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Move the package to the horizon-plugins subgroup on Salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2020 16:08:38 +0200

mistral-dashboard (9.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 21:13:22 +0200

mistral-dashboard (9.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2019 08:59:00 +0200

mistral-dashboard (8.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.
  * Bump Standards-Version to 4.4.0.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 22 Aug 2019 12:44:24 +0200

mistral-dashboard (8.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 12:02:23 +0200

mistral-dashboard (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions when available in Buster.
  * Standards-Version: 4.3.0 (no change).

 -- Thomas Goirand <zigo@debian.org>  Sun, 31 Mar 2019 22:17:38 +0200

mistral-dashboard (7.0.1-2) unstable; urgency=medium

  * Redesign mistral-dashboard:
      - Enabled files now in /etc/openstack-dashboard/
      - Removed post scripts which is now achieved by a trigger
  * d/copyright: Update copyright
  * d/control: Add me to uploaders field
  * d/control: Bump debhelper to 10
  * d/compat: Bump debhelper to 10

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 21 Jan 2019 22:10:26 +0100

mistral-dashboard (7.0.1-1) unstable; urgency=medium

  * Initial release (Closes: #840319).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Oct 2016 16:35:54 +0200
